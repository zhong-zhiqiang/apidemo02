// pages/devapidemo/devapidemo.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgsrc:[],
    index:3

  },
  chooseimageHandler(){
    wx.chooseImage({
      success:(result)=>{
        this.setData(
          {imgsrc:result.tempFilePaths},
          console.log(result)
        )
      },
     
    })
  },
  scanHandler(){
    wx.scanCode({
      onlyFromCamera: true,
      scanType:['barCode','qrCode'],
      success:(result)=>{
        console.log(result);
      },
    })
  },
  makePhoneCallHandler(){
    wx.makePhoneCall({
      phoneNumber: '119',
      success: (res) => {},
      fail: (res) => {},
      complete: (res) => {},
    })

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    wx.onAccelerometerChange((result) => {
      if(result.x > 1&& result.y > 1){
        console.log(result);
        let num=(this.data.index +1)% this.data.imgsrc.length
        this.setData({
          index:num
        })
      }
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})