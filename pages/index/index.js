// pages/index/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imglist:['/assets/images/qs1.GIF','/assets/images/qs2.GIF','/assets/images/qs3.GIF','/assets/images/qs4.GIF'],
    runname:['呼啦圈','举重','跳绳','哑铃'],
    index:0,//当前动作编号
    count:8,//当前动作倒计时
    _totoal:8,//每个动作最多时间，单位是秒
    _timer:null//定时器

  },
  changeTime(){
    this.data._timer = setTimeout(()=>{
      this.setData({
        count:this.data.count -1
      })
      if(this.data.count>0){
        this.changeTime()
      }else{
        //一个动作已完成，换下个动作
        this.setData({
          index:this.data.index+1,
          count:this.data._totoal
        })
        if(this.data.index >= 4){
          wx.showModal({
            cancelText: '休息一下',
            confirmText:'继续运动',
            content:'运动已完成',
            showCancel:true,
            title:'提示',
            success:(result)=>{
              if(result.cancel){
                wx.navigateTo({
                  url: '/pages/first/first.js',
                })
              }else if(result.confirm){
                this.setData({index:0,count:8})
              }
            }
          })
        }
        this.changeTime()


      }

    },1000);
   

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
   
   
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    //页面启动则显示计时器
    this.changeTime()

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {//页面隐藏则停止计时器
    clearTimeout(this.data_timer)

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})